import pygame
from pygame.locals import *

from pytmx import *
from pytmx.util_pygame import load_pygame

# ===================================================================
class TileMap(object):
    def __init__(self, filename):
        tm = load_pygame(filename)
        
        # self.size will be the pixel size of the map
        # this value is used later to render the entire map to a pygame surface
        self.pixel_size = tm.width * tm.tilewidth, tm.height * tm.tileheight
        self.tmx_data = tm
        
        #locate tile layer
        for layer in self.tmx_data.visible_layers:
            if isinstance(layer, TiledTileLayer):
                self.tile_layer = layer
        
    def render_map(self, surface, origin):
        # iterate over all the visible layers, then draw them
        for layer in self.tmx_data.visible_layers:
            if isinstance(layer, TiledTileLayer):
                self.render_tile_layer(surface, layer, origin)

            # elif isinstance(layer, TiledObjectGroup):
                # self.render_object_layer(surface, layer)

            # elif isinstance(layer, TiledImageLayer):
                # self.render_image_layer(surface, layer)
                
    def render_tile_layer(self, surface, layer, origin):
        # deref these heavily used references for speed
        tw = self.tmx_data.tilewidth
        th = self.tmx_data.tileheight
        surface_blit = surface.blit
        ox, oy = origin
        
        # iterate over the tiles in the layer
        for x, y, image in layer.tiles():
            surface_blit(image, ((x * tw) - ox, (y * th) - oy))
            
    def collide_rect(self, rect):
        x_min = int(rect.x / self.tmx_data.tilewidth)
        x_max = int((rect.x + rect.w) / self.tmx_data.tilewidth)
        y_min = int(rect.y / self.tmx_data.tileheight)
        y_max = int((rect.y + rect.h) / self.tmx_data.tileheight)
        
        if x_min < 0:
            x_min = 0
        if y_min < 0:
            y_min = 0
            
        if x_max >= self.tmx_data.width:
            x_max = self.tmx_data.width - 1
        if y_max >= self.tmx_data.height:
            y_max = self.tmx_data.height - 1
        
        for x_ind in range(x_min, x_max + 1):
            for y_ind in range(y_min, y_max + 1):
                p = self.tmx_data.get_tile_properties(x_ind, y_ind, 0)
                if p is not None:
                    if 'collision' in p:
                        if p['collision'] == 'true':
                            return True
        return False

    def at_exit(self, rect):
        x_min = int(rect.x / self.tmx_data.tilewidth)
        x_max = int((rect.x + rect.w) / self.tmx_data.tilewidth)
        y_min = int(rect.y / self.tmx_data.tileheight)
        y_max = int((rect.y + rect.h) / self.tmx_data.tileheight)

        if x_min < 0:
            x_min = 0
        if y_min < 0:
            y_min = 0

        if x_max >= self.tmx_data.width:
            x_max = self.tmx_data.width - 1
        if y_max >= self.tmx_data.height:
            y_max = self.tmx_data.height - 1

        for x_ind in range(x_min, x_max + 1):
            for y_ind in range(y_min, y_max + 1):
                p = self.tmx_data.get_tile_properties(x_ind, y_ind, 0)
                if p is not None:
                    if 'ladder' in p:
                        if p['ladder'] == 'true':
                            return True
        return False

