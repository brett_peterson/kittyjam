import pygame

# ===================================================================
class Command:
    def execute(toon):
        print("uh-oh, you didn't override this in the child class")

# ===================================================================
class MoveCommand(Command):
    def __init__(self, direction):
        self.dir = direction
        
    def execute(self, toon):
        toon.move(self.dir)

# ===================================================================
class ACommand(Command):
    def execute(self, toon):
        if toon.primary_ability:
            toon.primary_ability.start()

        # ===================================================================
class BCommand(Command):
    def execute(self, toon):
        if toon.secondary_ability:
            toon.secondary_ability.start()
