import pygame
import Actor
import TileMap
import Camera
import os.path
import Particle
import GameSeriesData as gsd

# ===================================================================
class _GameState:
    def __init__(self):
        #new game - load default game state
        self.victory = False
        self.defeat = False
        
    def victorious(self):
        return self.victory
        
    def defeated(self):
        return self.defeat
    
# ===================================================================
class LevelBase(object):

    def __init__(self, game_series_data, spawn_coords, monster_type, spawn_monster_location, map_file):
        self.all_players_exited = False
        self.game_state = _GameState()
        self.players = []
        self.monster = None
        self.game_series_data = game_series_data
        player_count = len(game_series_data.joystick_to_player_list)
        if player_count < 2:
            self.setup_dev_mode(spawn_coords[0], monster_type, spawn_monster_location, game_series_data)
        else:
            for index in range(player_count):
                if index == game_series_data.monster_player:
                    self.monster = monster_type(
                        spawn_monster_location[0], spawn_monster_location[1],
                        game_series_data.player_colors[index], 6, self
                    )
                    self.players.append(self.monster)
                else:
                    self.players.append(Actor.Hero(
                        spawn_coords[index][0], spawn_coords[index][1],
                        game_series_data.player_colors[index], 0, self
                    ))

        self.tilemap = TileMap.TileMap(os.path.join('data', 'Maps', map_file))
        self.camera = Camera.Camera(pygame.Rect(0, 0, 1920, 1080), self)

    def setup_dev_mode(self, player_pos, monster_type, monster_pos, game_series_data):
        print "Not enough players, DEV MODE ENGAGED!"
        hero = Actor.Hero(
            player_pos[0], player_pos[1],
            game_series_data.player_colors[0],
            0, self
        )
        self.players.append(hero)
        self.monster = monster_type(
            monster_pos[0], monster_pos[1],
            game_series_data.player_colors[1],
            6, self
        )
        self.players.append(self.monster)
        if gsd.KEYBOARD_JOYSTICK_ID not in game_series_data.joystick_to_player_list:
            game_series_data.add_player(gsd.KEYBOARD_JOYSTICK_ID)
        game_series_data.add_player(gsd.DEV_MODE_MONSTER_JOYSTICK_ID)
        num_players = len(game_series_data.joystick_to_player_list)
        end_plural = ""
        if num_players > 1:
            end_plural = "s"
        pygame.display.set_caption(
            "KittyJam - Monster Escape: DEV MODE (%d player%s)" % (num_players, end_plural)
        )

    def get_heroes(self):
        return self.players
        
    def update(self):
        num_players_exited = 0
        for p in self.players:
            p.update()
            if  Actor._STATE.EXITED == p.get_state() or \
                Actor._STATE.DEAD == p.get_state():
                num_players_exited += 1
        #load the new map if all players have reached the exit
        if num_players_exited == len(self.players)-1 :
            self.all_players_exited = True
            #scoring
            for index, p in enumerate( self.players, start = 0 ):
                if Actor._STATE.EXITED == p.get_state():
                    self.game_series_data.player_scores[index] += 1
                if Actor._STATE.DEAD == p.get_state():
                    self.game_series_data.player_scores[self.game_series_data.monster_player] += 1

        #self.camera.update()


    def players_exited(self):
        return self.all_players_exited

    def render(self, screen):
        self.tilemap.render_map(screen, self.camera.get_origin())
        
        for p in self.players:
            p.render(screen, self.camera.get_origin())



# ===================================================================
class Level1(LevelBase):

    def __init__(self, game_series_data):
        LevelBase.__init__(self, game_series_data,
            [[200, 50], [200, 100], [300, 50], [300, 100]], #  player_spawn_coords
            Actor.Zombie,
            (1850, 50),  # monster_spawn_coords
            "test_mapA.tmx"
        )

class Level2(LevelBase):

    def __init__(self, game_series_data):
        LevelBase.__init__(self, game_series_data,
            [[50, 120], [200, 120], [300, 50], [300, 100]], #  player_spawn_coords
            Actor.Demigorgon,
            (1850, 1000),                                     # monster_spawn_coords
            "Map3_16_16_a.tmx"
        )

class Level3(LevelBase):

    def __init__(self, game_series_data):
        LevelBase.__init__(self, game_series_data,
            [[200, 50], [200, 100], [300, 50], [300, 100]], #  player_spawn_coords
            Actor.Trex,
            (1000, 400),  # monster_spawn_coords
            "test_mapB.tmx"
        )

        self.snow = Particle.Emitter()
        self.snow.add_factory(Particle.Snow(), 400)
        self.snow.run()

    def render(self, screen):
        super(Level3, self).render(screen)

        self.snow.render(screen)

    def update(self):
        super(Level3, self).update()
        self.snow.update()
