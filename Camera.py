import pygame

class Camera(object):
    def __init__(self, rect, game_instance):
        self.rect = rect
        self.game_instance = game_instance
        self.origin = (0, 0)
        self.move_margin = 100
        
    def get_origin(self):
        return self.origin
        
    def update(self):
        game = self.game_instance
        # move the camera if player(s) are near one edge
        ox, oy = self.origin
        dx = 0
        dy = 0
        
        x_min = self.rect.w
        x_max = 0
        y_min = self.rect.h
        y_max = 0
        
        for p in game.get_heroes():
            tmp_rect = p.get_rect().move(-ox, -oy)
            if tmp_rect.x < x_min:
                x_min = tmp_rect.x
            if tmp_rect.y < y_min:
                y_min = tmp_rect.y
            if tmp_rect.x + tmp_rect.w > x_max:
                x_max = tmp_rect.x + tmp_rect.w
            if tmp_rect.y + tmp_rect.h > y_max:
                y_max = tmp_rect.y + tmp_rect.h
        
        # update x direction
        buffer = 50
        if (x_min < self.move_margin) and (x_max < self.rect.w - self.move_margin - buffer):
            #move left
            dx = -2#x_min - self.move_margin
            
        if (x_max > self.rect.w - self.move_margin) and (x_min > self.move_margin + buffer):
            #move right
            dx = 2#x_max - (self.rect.w - self.move_margin)
            
        # update y direction
        if (y_min < self.move_margin) and (y_max < self.rect.h - self.move_margin - buffer):
            #move up
            dy = -2#y_min - self.move_margin
            
        if (y_max > self.rect.h - self.move_margin) and (y_min > self.move_margin + buffer):
            #move down
            dy = 2#y_max - (self.rect.h - self.move_margin)
        
        #make sure our camera fits within the bounds of our map
        map_x, map_y = game.tilemap.pixel_size
        ox = ox + dx
        if ox < 0:
            ox = 0
        if ox + self.rect.w > map_x:
            ox = map_x - self.rect.w
            
        oy = oy + dy
        if oy < 0:
            oy = 0
        if oy + self.rect.h > map_y:
            oy = map_y - self.rect.h
        
        self.origin = (ox, oy)
    
    def collide_rect(self, rect):
        ox, oy = self.origin
        r = rect.move(-ox, -oy)
        
        if (r.x < 0) or (r.y < 0) or ((r.x + r.w) > self.rect.w) or ((r.y + r.h) > self.rect.h):
            return True
        
        return False
