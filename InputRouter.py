import os
import pygame
import json
import Command
import GameSeriesData as gsd

# ===================================================================
class InputRouter:
    def __init__(self):
        # ----------------------------------
        # set up joypads
        # ----------------------------------

        self.button_map = []
        for c in range(4):
            # set default values for joypad buttons
            self.button_map.append({0:"A", 1:"B", 2:"X", 3:"Y", 4:"L", 5:"R", 6:"SE", 7:"ST"})

        # load input config file or create a new one
        fpath = os.path.join("data", "joycon_config.json")
        try:
            with open(fpath, 'r+') as f:
                self.button_map = json.load(f)

        except IOError:
            # No input config file, write defaults to the file
            with open(fpath, 'w') as f:
                json.dump(self.button_map, f, indent=2)
        
        self.joy_direction = []
        for j in range(len(self.button_map)):
            self.joy_direction.append([0,0])
            
        # ----------------------------------
        # set up keyboard
        # ----------------------------------
        self.key_direction = [0, 0]
        self.key_map = {pygame.K_w:"UP",
                        pygame.K_a:"LEFT",
                        pygame.K_s:"DOWN",
                        pygame.K_d:"RIGHT",
                        pygame.K_k:"ACOMMAND",
                        pygame.K_o:"BCOMMAND"}

    # ---------------------------------------
    def _send_keyboard_command(self, cmd_str):
        k_command = None
        self.key_direction = [0, 0]

        if cmd_str == "UP" or cmd_str == "N_DOWN" or \
            cmd_str =="DOWN" or cmd_str == "N_UP" or \
            cmd_str == "LEFT" or cmd_str == "N_RIGHT" or \
            cmd_str == "RIGHT" or cmd_str == "N_LEFT":
            # figure out which key was pressed and set the direction accordingly
            if pygame.key.get_pressed()[pygame.K_w]:
                self.key_direction[1] -= 1
            if pygame.key.get_pressed()[pygame.K_a]:
                self.key_direction[0] -= 1
            if pygame.key.get_pressed()[pygame.K_s]:
                self.key_direction[1] += 1
            if pygame.key.get_pressed()[pygame.K_d]:
                self.key_direction[0] += 1
            k_command = Command.MoveCommand(self.key_direction)
        elif cmd_str == "ACOMMAND":
            k_command = Command.ACommand()
        elif cmd_str == "BCOMMAND":
            k_command = Command.BCommand()
        return k_command
    
    # ---------------------------------------
    def get_mapped_function(self, event):
        joy_num = None
        mapped_function = None
        if event.type == pygame.JOYBUTTONDOWN:
            if (str(event.button) in self.button_map[event.joy].keys()):
                joy_num = event.joy
                mapped_function = self.button_map[event.joy][str(event.button)] + "_PRESS"
            else:
                print "button " + str(event.button) + " not supported"
        
        elif event.type == pygame.JOYBUTTONUP:
            if (str(event.button) in self.button_map[event.joy].keys()):
                joy_num = event.joy
                mapped_function = self.button_map[event.joy][str(event.button)] + "_RELEASE"

        return joy_num, mapped_function
        
    # ---------------------------------------
    def get_command(self, event):
        joy_num = None
        input_command = None
        
        # ----------------------------------
        # process joycon events
        # ----------------------------------
        if event.type == pygame.JOYHATMOTION:
            joy_num = event.joy
            if event.hat == 0:
                self.joy_direction[joy_num][0] = event.value[0]
                self.joy_direction[joy_num][1] = -event.value[1]
            
            input_command = Command.MoveCommand(self.joy_direction[joy_num])
            
        elif event.type == pygame.JOYBUTTONDOWN:
            joy_num = event.joy
            function = self.get_mapped_function(event)
            if function[1] == "A_PRESS":
                input_command = Command.ACommand()
            elif function[1] == "B_PRESS":
                input_command = Command.BCommand()
            elif function[1] == "X_PRESS":
                pass
            elif function[1] == "Y_PRESS":
                pass
            elif function[1] == "L_PRESS":
                pass
            elif function[1] == "R_PRESS":
                pass
            elif function[1] == "SE_PRESS":
                pass
            elif function[1] == "ST_PRESS":
                pass
        
        # ----------------------------------
        # process keyboard events
        # ----------------------------------
        if event.type == pygame.KEYDOWN:
            joy_num = gsd.KEYBOARD_JOYSTICK_ID
            if event.key in self.key_map.keys():
                input_command = self._send_keyboard_command(self.key_map[event.key])
        elif event.type == pygame.KEYUP:
            joy_num = gsd.KEYBOARD_JOYSTICK_ID
            if event.key in self.key_map.keys():
                input_command = self._send_keyboard_command("N_" + self.key_map[event.key])

        return joy_num, input_command
