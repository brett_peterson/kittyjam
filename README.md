# README #

### Project Dependencies ###

* Python 2.7
* PyGame 1.9.1 - for Python 2.7 (http://www.pygame.org/download.shtml)
* PyTmx (https://github.com/bitcraft/PyTMX) - install with pip


Bug list:
wrong text (Keyboard) after finishing restarting games
monster effects not hitting all players

Feature list:
add spawning points
theme music