import pygame
import json

# ===================================================================
class AnimatedSprite(pygame.sprite.Sprite):
    def __init__(self, config_file):
        super(AnimatedSprite, self).__init__() #call Sprite initializer
        self.index = 0
        self.current_delay = 0
        self.num_frames = []
        self.animation_num = 0
        self.finishCallback = None

        # load data from config file
        infile = open(config_file)

        # parse json data
        anim_data = json.load(infile)

        self._W = anim_data['width']
        self._H = anim_data['height']
        self.delay_frames = anim_data['delay_frames']
        self.num_animations = len(anim_data['animations'])
        for anim in anim_data['animations']:
            self.num_frames.append(anim['frames'])

        # load image file
        self.animation_surface = pygame.image.load(anim_data['path']).convert()

        # pygame.Sprite
        self.rect = pygame.Rect(0,0,self._W, self._H)
        self.image = pygame.Surface([self._W, self._H])
        if 'colorkey' in anim_data:
            colorkey = anim_data['colorkey']

            self.image.set_colorkey((colorkey['r'], colorkey['g'], colorkey['b']))
        else:
            self.image.set_colorkey((0, 0, 0))

    def update(self):
        self.current_delay += 1
        if self.current_delay > self.delay_frames:
            self.current_delay = 0
            # update index only after each delay
            self.index += 1
            if self.index >= self.num_frames[self.animation_num]:
                self.index = 0
                if self.finishCallback != None:
                    self.finishCallback()

    def get_num_animations(self):
        return self.num_animations

    def set_animation(self, animation_num, finishCallback = None):
        self.current_delay = 0
        self.index = 0
        self.animation_num = animation_num
        self.finishCallback = finishCallback

    def render(self, surface, render_rect, vertical_flip=False, horizontal_flip=False):
        #image = pygame.Surface([self._W, self._H])
        self.image.fill( (0,0,0) )
        self.image.blit(self.animation_surface, self.rect, pygame.Rect(self._W * self.index, self._H * self.animation_num, self._W, self._H))
        if (vertical_flip or horizontal_flip):
            self.image = pygame.transform.flip(self.image, vertical_flip, horizontal_flip)
        surface.blit(pygame.transform.scale(self.image, (render_rect.w, render_rect.h)), render_rect)
