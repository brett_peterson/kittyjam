import pygame
import random
import Game
import Scene

# Sentinel value for the keyboard & the monster in
# dev mode. This is so that we can receive & direct
# events from the keyboard, & so that we can be sure
# that scoring works for the monster in dev mode
KEYBOARD_JOYSTICK_ID = -1
DEV_MODE_MONSTER_JOYSTICK_ID = -2

class GameSeriesData(object):

    def __init__(self, gamepads):
        # represents the mapping between joystick indices
        # and player indices
        #
        # player index is the same as the list index
        # value AT that index is the joystick index in pygame
        #
        # for instance: index 1 (player 2) could be mapped to joystick 4
        self.joystick_to_player_list = [] # keyboard player is a None value @ index 0
        self.monster_player = None
        self.player_scores = []
        self.player_colors = [
            pygame.Color(255,   0,   0), # Red
            pygame.Color(  0,   0, 255), # Blue
            pygame.Color(  0, 255, 255), # GB - Teal
            pygame.Color(255,   0, 255), # RB - Pink
            pygame.Color(255, 255,   0), # RG - Yellow
            pygame.Color(  0, 255,   0), # Green
            pygame.Color(255, 128,   0), # Orange
            pygame.Color(127,   0, 255), # Purple
        ]
        self.max_players = len(self.player_colors)
        self.gamepads = gamepads
        self.levels = [Game.Level1, Game.Level2, Scene.Credits]
        #self.levels = [Game.Level1, Game.Level2, Game.Level3, Scene.Credits]


        lines = [line.rstrip('\n') for line in open('data/character_names.txt')]
        self.player_names = []
        self.used_nameIDs = []
        count = len( lines )
        random.seed()
        while len( lines ) <= self.max_players:
            line.append( "Player " + str(count) )
            count += 1
        while len( self.player_names ) != self.max_players:
            id = random.randint(0, len(lines) - 1)
            if id in self.used_nameIDs:
                pass
            else:
                self.player_names.append( lines[ id ] )
                self.used_nameIDs.append(id)



    def add_player(self, joystick_id):
        ''' Adds a player to the game series data

            Returns the number of players after adding current one
        '''
        self.joystick_to_player_list.append(joystick_id)
        self.player_scores.append(0)
        return len(self.player_scores)

    def choose_monster(self):
        if len(self.player_scores) < 3:
            self.monster_player = 1
            print "DEV MODE: Monster Selection Hard-Coded to Index 1"
            return
        random.seed()
        self.monster_player = random.randint(0, len(self.player_scores) - 1)
        print "Monster chosen as player %d (joystick %d)" % (
            self.monster_player,
            self.joystick_to_player_list[self.monster_player]
        )

    def __repr__(self):
        result_string = "GameSeriesData Instance:"
        result_string += "\n- Joystick IDs per player (index): %s" % self.joystick_to_player_list
        result_string += "\n- Player Scores: %s" % self.player_scores
        result_string += "\n- Monster Index: %d" % self.monster_player
        return result_string
