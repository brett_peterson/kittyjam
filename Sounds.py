import os
from pygame import mixer
from functools import wraps

class SoundEffect(mixer.Sound):
    def __init__(self, filename):
        super(SoundEffect, self).__init__(os.path.join('data', 'Sounds', filename))


# Example call outside subclass
# pygame.mixer.init()
# theme_song = pygame.mixer.Sound(os.path.join('data', 'Sounds', 'rainstorm.ogg'))
# theme_song.play()


class Audio(mixer.Sound):
    def __init__(self, sound):
        super(Audio, self).__init__(os.path.join('data', 'Sounds', sound))
        self.name = str(sound).replace('.ogg', '')


# does not require a mixer.init() call.
# sounds may overlap, play until completion
def effect(sound):

    def wrapper(func):

        @wraps(func)
        def noisemaker(*args, **kwargs):
            sfx = Audio(sound)
            call = func(*args, **kwargs)
            sfx.play()
            return call

        return noisemaker
    return wrapper
