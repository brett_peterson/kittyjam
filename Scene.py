import pygame
import Actor
import InputRouter
import Game
import Camera
import Particle
import GameSeriesData as gsd

# ===================================================================
class SceneBase:
    def __init__(self, game_series_data):
        self.next = self
        self.game_series_data = game_series_data

    def process_input(self, events):
        print "uh-oh, you didn't override this in the child class"

    def update(self):
        print "uh-oh, you didn't override this in the child class"

    def render(self, screen):
        print "uh-oh, you didn't override this in the child class"

    def switch_to_scene(self, next_scene):
        self.next = next_scene

# ===================================================================
class TitleScene(SceneBase):
    def __init__(self, game_series_data):
        SceneBase.__init__(self, game_series_data)
        font_player = pygame.font.Font(None, 30)
        self.background = pygame.image.load('data/ArtAssets/KityJam_OpenScreen.jpg')
        self.input_router = InputRouter.InputRouter()
        self.text = font_player.render("- Press Start To Begin -", True, (0, 255, 0))

        self.snow = Particle.Emitter()
        self.snow.add_factory(Particle.Snow(), 400)
        self.snow.run()

    def process_input(self, events):
        for event in events:
            # Handle keyboard events -----------------
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.event.post(pygame.event.Event(pygame.QUIT))
                elif event.key == pygame.K_RETURN:
                    self.switch_to_scene(ControllerSelectScene(self.game_series_data))
            # Handle joypad events -------------------
            elif event.type == pygame.JOYBUTTONDOWN:
                _, command = self.input_router.get_mapped_function(event)
                if command == "ST_PRESS":
                    self.switch_to_scene(ControllerSelectScene(self.game_series_data))
    
    def update(self):
        self.snow.update()
    
    def render(self, screen):
        screen.fill((0, 0, 0))
        x, y = screen.get_size()
        screen.blit(self.background, (0, 0))
        screen.blit(self.text, (((x - self.text.get_width()) / 2, y - ( y / 4) )) )
        self.snow.render(screen)
# ===================================================================
class ControllerSelectScene(SceneBase):
    def __init__(self, game_series_data):
        SceneBase.__init__(self, game_series_data)
        font_player = pygame.font.Font(None, 30)
        self.background = pygame.image.load('data/ArtAssets/KityJam_OpenScreen.jpg')
        self.font_controller = pygame.font.Font(None, 20)
        self.input_router = InputRouter.InputRouter()
        self.text_noplayer = font_player.render("- Press Start -", True, (0, 255, 0))
        player_colors = self.game_series_data.player_colors
        self.players_Fonts = []
        self.joystick_text = []
        for i in range(self.game_series_data.max_players):
            surface = font_player.render(self.game_series_data.player_names[i], True, player_colors[i])
            alternate_surface = None
            if i == 0:
                alternate_name = self.font_controller.render("(keyboard)", True, player_colors[0])
            self.joystick_text.append(  [surface, alternate_name]  )
    
        self.snow = Particle.Emitter()
        self.snow.add_factory(Particle.Snow(), 400)
        self.snow.run()

        self.particle_emitters = []
        for i in range(self.game_series_data.max_players):
            self.particle_emitters.append(Particle.Emitter((222 + 50 + i*100, 900)))
            self.particle_emitters[i].add_factory(Particle.BubbleBlower(player_colors[i]))

    def trigger_particle_emitter(self, joystick_id, start):
        player_list = self.game_series_data.joystick_to_player_list
        if joystick_id in player_list:
            player = player_list.index(joystick_id)
            player_particle_emitter = self.particle_emitters[player]
            if start:
                player_particle_emitter.run()
            else:
                player_particle_emitter.stop()

    
    def process_input(self, events):
        for event in events:
            # Handle keyboard events -----------------
            if event.type == pygame.KEYDOWN:
                print "keyboard event: %s" % event
                if event.key == pygame.K_ESCAPE:
                    pygame.event.post(pygame.event.Event(pygame.QUIT))
                elif event.key == pygame.K_RETURN:
                    self.game_series_data.choose_monster()
                    self.switch_to_scene(GameScene(self.game_series_data, Game.Level1(self.game_series_data)))
                elif event.key == pygame.K_a:
                    player_num = self.game_series_data.add_player(-1)
                    player_index = player_num - 1
                    player_color = self.game_series_data.player_colors[player_index]
                    self.joystick_text[player_index][1] = self.font_controller.render(
                        "keyboard", False, player_color
                    )
                elif event.key == pygame.K_o:
                    self.trigger_particle_emitter(gsd.KEYBOARD_JOYSTICK_ID, True)
            elif event.type == pygame.KEYUP:
                if event.key == pygame.K_o:
                    self.trigger_particle_emitter(gsd.KEYBOARD_JOYSTICK_ID, False)


            # Handle joypad events -------------------
            elif event.type == pygame.JOYBUTTONDOWN:
                joy_num, command = self.input_router.get_mapped_function(event)
                if command == "ST_PRESS":
                    print event
                    if event.joy not in self.game_series_data.joystick_to_player_list:
                        player_num = self.game_series_data.add_player(event.joy)
                        for joystick in self.game_series_data.gamepads:
                            if event.joy == joystick.get_id():
                                player_index = player_num - 1
                                player_color = self.game_series_data.player_colors[player_index]
                                self.joystick_text[player_index][1] = self.font_controller.render(
                                    joystick.get_name(), False, player_color
                                )
                        print "joystick %d is now player %d" % (event.joy, player_num)
                elif command == "A_PRESS":
                    self.trigger_particle_emitter(joy_num, True)

            elif event.type == pygame.JOYBUTTONUP:
                joy_num, command = self.input_router.get_mapped_function(event)
                if command == "A_RELEASE":
                    self.trigger_particle_emitter(joy_num, False)

    def update(self):
        for p in self.particle_emitters:
            p.update()
        self.snow.update()

    def render(self, screen):
        x, y = screen.get_size()
        screen.blit(self.background, (0, 0))
        x_half = x / 2
        y_half = y / 2
        left_x = (x - self.text_noplayer.get_width()) / 8
        bottom_y = y - (4 * self.text_noplayer.get_height())
        bottom_subtitle_y = bottom_y + 30
        player_number = len(self.game_series_data.joystick_to_player_list)
        left_x = left_x + 100
        for i in range(player_number):

            screen.blit(self.joystick_text[i][0], (left_x, bottom_y))
            self.particle_emitters[i].pos = ( left_x + self.joystick_text[i][0].get_width()/2, self.particle_emitters[i].pos[1])
            if self.joystick_text[i][1] is not None:

                screen.blit(self.joystick_text[i][1], (left_x, bottom_subtitle_y))
            left_x += self.joystick_text[i][0].get_width() + 100
        screen.blit(self.text_noplayer, (x_half - 100, y - y / 4 ))

        for p in self.particle_emitters:
            p.render(screen)
        self.snow.render(screen)

# ===================================================================
class GameScene(SceneBase):
    def __init__(self, game_series_data, game_instance):
        SceneBase.__init__(self, game_series_data)
        #create game objects
        self.input_router = InputRouter.InputRouter()
        self.game_instance = game_instance
        print "GameScene.init: game series data: %s" % game_series_data

    def process_input(self, events):
        for event in events:
            # Handle joypad events -------------------
            # Handle keyboard events -----------------
            if event.type == pygame.JOYBUTTONDOWN or \
               event.type == pygame.JOYAXISMOTION or \
               event.type == pygame.JOYHATMOTION or \
               event.type == pygame.KEYDOWN or \
               event.type == pygame.KEYUP:
                joystick_id, command = self.input_router.get_command(event)
                if command is None:
                    continue
                heroes = self.game_instance.get_heroes()
                joystick_to_player_list = self.game_series_data.joystick_to_player_list
                if joystick_id is not None and joystick_id in joystick_to_player_list:
                    player_index = joystick_to_player_list.index(joystick_id)
                    command.execute(heroes[player_index])
        
    def update(self):
        self.game_instance.update()
        if self.game_instance.players_exited():
            self.switch_to_scene(ScoreScene(self.game_series_data))
    
    def render(self, screen):
        self.game_instance.render(screen)

# ===================================================================
class ScoreScene(SceneBase):
    def __init__(self, game_series_data):
        SceneBase.__init__(self, game_series_data)
        font = pygame.font.Font(None, 30)
        self.background = pygame.image.load('data/ArtAssets/KityJam_OpenScreen.jpg')
        self.input_router = InputRouter.InputRouter()
        print "GameScene.init: game series data: %s" % game_series_data
        self.game_series_data.levels.pop(0)
        if len(self.game_series_data.levels) > 0:
            self.next_scene = self.game_series_data.levels[0](self.game_series_data)
        else:
            print "ran out of levels"


        self.text_noplayer = font.render("- Press Start To Continue -", True, (0, 255, 0))
        player_colors = self.game_series_data.player_colors

        self.joystick_text = []
        monster_index = self.game_series_data.monster_player
        for index, p in enumerate(self.game_series_data.player_scores, start=0):
            #title = "Player " + str(index + 1)
            title = self.game_series_data.player_names[index]
            if monster_index == index:
                title = "Monster"
            text = font.render( title + "'s score: " + str(p) , True, player_colors[index] )
            self.joystick_text.append( text )

    def process_input(self, events):
        for event in events:
            # Handle keyboard events -----------------
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.event.post(pygame.event.Event(pygame.QUIT))
                elif event.key == pygame.K_RETURN:
                    print self.game_series_data.levels
                    if len(self.game_series_data.levels) == 1:
                        self.switch_to_scene(Credits(self.game_series_data))
                    else:
                        self.switch_to_scene(GameScene(self.game_series_data, self.next_scene))
            # Handle joypad events -------------------
            elif event.type == pygame.JOYBUTTONDOWN:
                _, command = self.input_router.get_mapped_function(event)
                if command == "ST_PRESS":
                    if len(self.game_series_data.levels) == 1:
                        self.switch_to_scene(Credits(self.game_series_data))
                    else:
                        self.switch_to_scene(GameScene(self.game_series_data, self.next_scene))

    def update(self):
        pass

    def render(self, screen):
        x, y = screen.get_size()
        screen.blit(self.background, (0, 0))
        bottom_y = y - (4 * self.text_noplayer.get_height())
        player_number = len(self.game_series_data.joystick_to_player_list)
        left_x = 100
        for i in range(player_number):
            screen.blit(self.joystick_text[i], (left_x , bottom_y))
            left_x += self.joystick_text[i].get_width() + 100

class Credits(SceneBase):
    def __init__(self, game_series_data):
        SceneBase.__init__(self, game_series_data)
        pygame.display.set_caption('End credits')
        self.font = pygame.font.SysFont("Arial", 40)
        self.screen_rect = pygame.Rect(0,0,1920,1056)
        self.credit_list = ["- CREDITS -", "KittyJam - Monster Escape", "",
                       "Kevin Imber", "Brett Peterson", "Steve Grover", "Tommy Koi", "Rishi Javia",
                       "Trevor Peterson",
                       "Matt Rodger", "Molly McGowan", "", "Honerable Mentions", "Danielle Deboer","Bradley Nelson", "",
                       "Dishonerable Mentions", "Hanako Imber"]
        self.texts = []
            # we render the text once, since it's easier to work with surfaces
            # also, font rendering is a performance killer
        for i, line in enumerate(self.credit_list):
            s = self.font.render(line, 1, (255, 215, 0))
            # we also create a Rect for each Surface.
            # whenever you use rects with surfaces, it may be a good idea to use sprites instead
            # we give each rect the correct starting position
            r = s.get_rect(centerx=self.screen_rect.centerx, y=self.screen_rect.bottom + i * 45)
            self.texts.append((r, s))

    def process_input(self, events):
        for event in events:
            # Handle keyboard events -----------------
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.event.post(pygame.event.Event(pygame.QUIT))
                elif event.key == pygame.K_RETURN:
                    self.switch_to_scene(TitleScene(self.game_series_data))

    def update(self):
        for r, _ in self.texts:
            r.move_ip(0, -1)

        if not self.screen_rect.collidelistall([r for (r, _) in self.texts]):
            self.switch_to_scene(TitleScene(self.game_series_data))

    def render(self, screen):
        screen.fill((0, 0, 0))
        for r, s in self.texts:
            screen.blit(s, r)
