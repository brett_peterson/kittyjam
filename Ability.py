import AnimatedSprite
from Sounds import SoundEffect
from pygame import mixer
import os
import math

class Ability(object):
    def __init__(self, cooldown):
        self.cooldown = cooldown
        self.cooldown_timer = 0
    
    def update(self):
        if self.cooldown_timer > 0:
            self.cooldown_timer -= 1
            
    def start(self):
        started = False
        if self.cooldown_timer == 0:
            self.cooldown_timer = self.cooldown
            started = True
        else:
            print "Ability on cooldown..."
        return started

    def render(self, surface, render_rect, flip):
        print("uh-oh, you didn't override this in the child class")

    def finish(self):
        print("uh-oh, you didn't override this in the child class")

class Whistle(Ability):

    def __init__(self, actor_obj):
        super(Whistle, self).__init__(60 * 5)
        self.actor_obj = actor_obj
        self.animation = AnimatedSprite.AnimatedSprite('data/whistle.json')
        self.animate = False
        self.duration = 60 * 1
        self.duration_timer = self.duration
        self.speed_penalty = 2
        self.whistling = False
        self.whistleSound = mixer.Sound(os.path.join('data', 'Sounds', 'whistle.ogg'))

    def start(self):
        if super(Whistle, self).start():
            self.monster = self.actor_obj.game_instance.monster
            self.animate = True
            self.whistling = True
            self.duration_timer = self.duration
            self.animation.set_animation(0, self.end_animation)
            self.monster.speed -= self.speed_penalty
            self.whistleSound.play()
            print("Hero Whistling")

    def render(self, surface, render_rect, flip):
        if self.animate:
            if flip:
                whistle_rect = render_rect.move(-render_rect.width, 0)
            else:
                whistle_rect = render_rect.move(render_rect.width, 0)
            self.animation.render(surface, whistle_rect)

    def end_animation(self):
        print "Animation over"
        self.animate = False
        
    def update(self):
        super(Whistle, self).update()
        if self.animate:
            self.animation.update()
        
        if self.whistling:
            if self.duration_timer > 0:
                self.duration_timer -= 1
            else:
                self.finish()
                
    def finish(self):
        self.whistling = False
        self.monster.speed += self.speed_penalty
        print("End Whistling")
        
        
class Sprint(Ability):
    def __init__(self, actor_obj):
        super(Sprint, self).__init__(60 * 5)
        self.speed_boost = 2
        self.duration = 60 * 1
        self.duration_timer = self.duration
        self.actor_obj = actor_obj
        self.sprinting = False

    def start(self):
        if super(Sprint, self).start():
            self.sprinting = True
            self.duration_timer = self.duration
            if self.actor_obj.speed > 0:
                self.actor_obj.speed += self.speed_boost
            else:
                self.actor_obj.speed -= self.speed_boost

            print("Sprinting")

    def render(self, surface, render_rect, flip):
        pass
      
    def update(self):
        super(Sprint, self).update()
        if self.sprinting:
            if self.duration_timer > 0:
                self.duration_timer -= 1
            else:
                self.finish()

    def finish(self):
        self.sprinting = False
        if self.actor_obj.speed > 0:
            self.actor_obj.speed -= self.speed_boost
        else:
            self.actor_obj.speed += self.speed_boost
        print("End Sprinting")

class InvertMovement(Ability):
    def __init__(self, players):
        super(InvertMovement, self).__init__(60 * 6)
        self.animation = AnimatedSprite.AnimatedSprite('data/rawr.json')
        self.animate = False
        self.duration = 60 * 3
        self.duration_timer = self.duration
        self.players = players
        self.active = False
        self.curseSound = mixer.Sound(os.path.join('data', 'Sounds', 'curse.ogg'))

    def start(self):
        if super(InvertMovement, self).start():
            self.animate = True
            self.active = True
            self.duration_timer = self.duration
            self.animation.set_animation(0, self.end_animation)
            for p in self.players:
                p.speed *= -1.0
            self.curseSound.play()
            print("movement inverted")

    def render(self, surface, render_rect, flip):
        if self.animate:
            # self.animation.animation_surface
            if flip:
                whistle_rect = render_rect.move(-render_rect.width, 0)
            else:
                whistle_rect = render_rect.move(render_rect.width, 0)
            self.animation.render(surface, whistle_rect)
      
    def update(self):
        super(InvertMovement, self).update()

        if self.animate:
            self.animation.update()
            
        if self.active:
            if self.duration_timer > 0:
                self.duration_timer -= 1
            else:
                self.finish()

    def end_animation(self):
        print "Animation over"
        self.animate = False

    def finish(self):
        self.active = False

        for p in self.players:
            p.speed *= -1.0
        print("movement normal")


class Rawr(Ability):
    def __init__(self, players):
        super(Rawr, self).__init__(60 * 5)
        self.players = players
        self.animation = AnimatedSprite.AnimatedSprite('data/rawr.json')
        self.animate = False
        self.duration = 60 * 1
        self.duration_timer = self.duration
        self.speed_penalty = 2
        self.rawring = False
        self.rawrSound = mixer.Sound(os.path.join('data', 'Sounds', 'rawr.ogg'))

    def start(self):
        if super(Rawr, self).start():
            self.animate = True
            self.rawring = True
            self.duration_timer = self.duration
            self.animation.set_animation(0, self.end_animation)
            for p in self.players:
                p.speed *= -1
            rawrSound.play()
            print("Monster Rawring")

    def render(self, surface, render_rect, flip):
        if self.animate:
            # self.animation.animation_surface
            if flip:
                whistle_rect = render_rect.move(-render_rect.width, 0)
            else:
                whistle_rect = render_rect.move(render_rect.width, 0)
            self.animation.render(surface, whistle_rect)

    def end_animation(self):
        print "Animation over"
        self.animate = False

    def update(self):
        super(Rawr, self).update()
        if self.animate:
            self.animation.update()

        if self.rawring:
            if self.duration_timer > 0:
                self.duration_timer -= 1
            else:
                self.finish()

    def finish(self):
        self.rawring = False
        for p in self.players:
            p.speed += 1
        print("End Rawring")