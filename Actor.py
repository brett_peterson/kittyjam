import pygame
import math
import AnimatedSprite
import Ability
from Sounds import effect

class _STATE(object):
    NORMAL = 0
    EXITED = 1
    DEAD = 2

class MOVEMENT_STATE(object):
    IDLE = 0
    WALKING = 1

class Actor(object):
    def __init__(self, x, y, game_instance, w, h):
        self.game_instance = game_instance
        self.w = w
        self.h = h
        self.rect = pygame.Rect(x, y, self.w, self.h)
        self.rect.center = (x,y)
        self.x = float(x)
        self.y = float(y)
        self.color = "RED"
        #velocity info
        self.speed = 1.8
        self.xdir = 0
        self.ydir = 0
        #game info
        self.state = _STATE.NORMAL
        self.last_movement_state = MOVEMENT_STATE.IDLE
        self.movement_state = MOVEMENT_STATE.IDLE
        self.flip = False
        self.max_HP = 100
        self.HP = self.max_HP
        self.canExit = False
        self.primary_ability = None
        self.secondary_ability = None
        
    def get_rect(self):
        return self.rect
        
    def get_pos(self):
        return int(self.x), int(self.y)

    def get_state(self):
        return self.state

    def render(self, surface, origin):
        ox, oy = origin
        hb_border = pygame.Rect(self.rect.x - 2 - ox, self.rect.y + self.h + 1 - oy, self.rect.w + 4, 5)
        hb_bg_rect = pygame.Rect(self.rect.x - 1 - ox, self.rect.y + self.h + 2 - oy, self.rect.w + 2, 3)

        pygame.draw.rect(surface, pygame.Color("GRAY"), hb_border, 1)
        pygame.draw.rect(surface, pygame.Color("BLACK"), hb_bg_rect)

        hp_width = int(self.HP * (self.rect.w + 2) / self.max_HP)
        hb_hp_rect = pygame.Rect(self.rect.x - 1 - ox, self.rect.y + self.h + 2 - oy, hp_width, 3)
        pygame.draw.rect(surface, self.color, hb_hp_rect)

        render_rect = self.rect.move(-ox, -oy)

        if self.primary_ability:
            self.primary_ability.render(surface, render_rect, self.flip)

        if self.secondary_ability:
            self.secondary_ability.render(surface, render_rect, self.flip)

    def update(self):
        # follow state machine
        if _STATE.NORMAL == self.state:
            pass
        elif _STATE.EXITED == self.state:
            pass
        elif _STATE.DEAD == self.state:
            pass
        else:
            print "INVALID STATE!!!"
        
        # Apply movement
        if self.xdir != 0 or self.ydir != 0:
            #store a copy of our x/y direction so we can roll
            #back the position if there is a collision
            collide_x = False
            collide_y = False
            at_exit = False

            dx = self.xdir * self.speed
            if dx > 0:
                dx_int = math.ceil(dx)
            else:
                dx_int = math.floor(dx)
            
            dy = self.ydir * self.speed
            if dy > 0:
                dy_int = math.ceil(dy)
            else:
                dy_int = math.floor(dy)
                
            temp_rect = self.rect.move(dx_int, 0)

            # check collisions in x direction
            if (self.game_instance.tilemap.collide_rect(temp_rect) == True) or \
               (self.game_instance.camera.collide_rect(temp_rect) == True):
                collide_x = True
            # check for exit in x direction
            if (self.game_instance.tilemap.at_exit(temp_rect) == True and self.canExit):
                self.state = _STATE.EXITED

            # update our x position and the temp rect
            if (collide_x == False):
                self.x = self.x + dx
                temp_rect.move_ip(0, dy_int)
            else:
                temp_rect = self.rect.move(0, dy_int)

            # check collisions in y direction
            if (self.game_instance.tilemap.collide_rect(temp_rect) == True) or \
               (self.game_instance.camera.collide_rect(temp_rect) == True):
                collide_y = True

            # check for exit in y direction
            if (self.game_instance.tilemap.at_exit(temp_rect) == True and self.canExit):
                self.state = _STATE.EXITED

            # update our y position
            if (collide_y == False):
                self.y = self.y + dy
            else:
                temp_rect.move_ip(0, -dy_int)

            self.rect.centerx = temp_rect.centerx
            self.rect.centery = temp_rect.centery
        
        
    def take_damage(self, caster, damage):
        if self.HP > damage:
            self.HP -= damage
        else:
            self.HP = 0
            self.die()
            
    def die(self):
        self.state = _STATE.DEAD
        self.xdir = 0
        self.ydir = 0

    # ---------------------------------
    # COMMAND INTERFACE
    # ---------------------------------
    def move(self, dir):
        if self.state != _STATE.DEAD:
            self.xdir = dir[0]
            self.ydir = dir[1]

        if self.xdir == -1:
            self.movement_state = MOVEMENT_STATE.WALKING
            self.flip = True
            #self.last_movement_state = self.movement_state
        if self.xdir == 1:
            self.movement_state = MOVEMENT_STATE.WALKING
            self.flip = False
            #self.last_movement_state = self.movement_state
        if self.xdir == 0 and self.ydir == 0:
            self.movement_state = MOVEMENT_STATE.IDLE

        if self.ydir == -1 or self.ydir == 1:
            self.movement_state = MOVEMENT_STATE.WALKING


        # ===================================================================
class Hero(Actor):
    def __init__(self, x, y, color, target_offset, game_instance):
        super(Hero, self).__init__(x, y, game_instance, 20, 24 )
        self.color = color
        self.target_offset = target_offset
        self.animation = AnimatedSprite.AnimatedSprite('data/warrior_cat.json')
        self.anim_num = 0
        self.canExit = True
        self.primary_ability = Ability.Sprint(self)
        self.secondary_ability = Ability.Whistle(self)

        self.animation.set_animation(self.anim_num)

    def render(self, surface, origin):
        if self.state == _STATE.NORMAL:
            #render Hero
            ox, oy = origin
            render_rect = self.rect.move(-ox, -oy)
            if self.game_instance.monster.rect.colliderect(self.rect):
                self.state = _STATE.DEAD

            if self.movement_state == MOVEMENT_STATE.WALKING:
                self.anim_num = 1
            if self.movement_state == MOVEMENT_STATE.IDLE:
                self.anim_num = 0

            if self.last_movement_state != self.movement_state:
                self.animation.set_animation(self.anim_num)
                self.last_movement_state = self.movement_state

            self.animation.render(surface, render_rect, self.flip)
            super(Hero, self).render(surface, origin)
        
    def update(self):
        self.secondary_ability.update()
        self.primary_ability.update()

        #handles movement
        self.animation.update()
        super(Hero, self).update()


class Monster(Actor):
    def __init__(self, x, y, color, target_offset, game_instance, w, h, sprite):
        super(Monster, self).__init__(x, y, game_instance, w, h)
        self.color = color
        self.target_offset = target_offset
        self.animation = AnimatedSprite.AnimatedSprite(sprite)
        self.anim_num = self.movement_anim_num()
        self.speed = 4

        self.animation.set_animation(self.anim_num)

    def render(self, surface, origin):
        # render Hero
        ox, oy = origin
        render_rect = self.rect.move(-ox, -oy)

        if self.last_movement_state != self.movement_state:
            self.animation.set_animation(self.movement_anim_num())
            self.last_movement_state = self.movement_state

        self.animation.render(surface, render_rect, self.flip)
        super(Monster, self).render(surface, origin)


    def movement_anim_num(self):
        # Return the index of the sprite for each MOVEMENT_STATE
        print("You need to add animations for your monster in it's subclass!")
        return 0

    def update(self):
        # handles movement
        self.animation.update()
        super(Monster, self).update()

class Zombie(Monster):
    def __init__(self, x, y, color, target_offset, game_instance):
        super(Zombie, self).__init__(x, y, color, target_offset, game_instance, 20, 24, 'data/zombie.json')
        self.speed = 3.0
        self.primary_ability = Ability.Sprint(self)

    def movement_anim_num(self):
        # Return the index of the sprite for each MOVEMENT_STATE
        if self.movement_state == MOVEMENT_STATE.WALKING:
            return 1
        elif self.movement_state == MOVEMENT_STATE.IDLE:
            return 0

    def update(self):
        super(Zombie, self).update()
        self.primary_ability.update()

class Demigorgon(Monster):
    def __init__(self, x, y, color, target_offset, game_instance):
        super(Demigorgon, self).__init__(x, y, color, target_offset, game_instance, 56, 60, 'data/demigorgon.json')
        self.speed = 4.0
        self.primary_ability = Ability.InvertMovement([x for x in game_instance.players if x != game_instance.monster])

    def movement_anim_num(self):
        # Return the index of the sprite for each MOVEMENT_STATE
        if self.movement_state == MOVEMENT_STATE.WALKING:
            return 0
        elif self.movement_state == MOVEMENT_STATE.IDLE:
            return 7
            
    def update(self):
        super(Demigorgon, self).update()
        self.primary_ability.update()

class Trex(Monster):
    def __init__(self, x, y, color, target_offset, game_instance):
        super(Trex, self).__init__(x, y, color, target_offset, game_instance, 60, 48, 'data/trex.json')
        self.speed = 4
        self.primary_ability = Ability.Rawr([x for x in game_instance.players if x != game_instance.monster])

    def movement_anim_num(self):
        # Return the index of the sprite for each MOVEMENT_STATE
        if self.movement_state == MOVEMENT_STATE.WALKING:
            return 0
        elif self.movement_state == MOVEMENT_STATE.IDLE:
            return 1

    def update(self):
        super(Trex, self).update()
        self.primary_ability.update()