import os, sys
import Scene
import pygame
import GameSeriesData as gsd
from pygame.locals import *
from pygame import mixer

# ------------------------------------------------------ #
# set up the game window, pygame modules, and load media #
# ------------------------------------------------------ #
position = 0, 10
os.environ['SDL_VIDEO_WINDOW_POS'] = str(position[0]) + "," + str(position[1])

#initialize the game window    
pygame.init()
mixer.init()
size = width, height = 1920, 1056
screen = pygame.display.set_mode(size)
pygame.display.set_caption("KittyJam - Monster Escape")
clock = pygame.time.Clock()

#mouse settings
# pygame.mouse.set_visible(False)

#joystick settings
if pygame.joystick.get_init():
    print "Joystick initialized"
print "num joysticks: " + str(pygame.joystick.get_count())

gamepads = []
for j in range(pygame.joystick.get_count()):
    gamepads.append(pygame.joystick.Joystick(j))
    gamepads[j].init()

#create active scene and initialize it to the title scene
game_series_data = gsd.GameSeriesData(gamepads)
active_scene = Scene.TitleScene(game_series_data)

#flush events - prevents stray inputs from the joypads
pygame.event.clear()

# ------------------------------------------------------ #
# Start main game loop
# ------------------------------------------------------ #
exit = False
while 1:
    # limit framerate to 60 fps
    clock.tick(60)
    
    # -------------------------------------------------- #
    # Handle all user input
    # -------------------------------------------------- #
    player_events = []
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit = True
            
        if event.type == pygame.KEYUP or \
           event.type == pygame.KEYDOWN or \
           event.type == pygame.MOUSEBUTTONUP or \
           event.type == pygame.MOUSEBUTTONDOWN or \
           event.type == pygame.JOYBUTTONDOWN or \
           event.type == pygame.JOYBUTTONUP or \
           event.type == pygame.JOYHATMOTION or \
           event.type == pygame.JOYAXISMOTION:
            # send events to the player's input handler
            player_events.append(event)

    if exit == True:
        break
    
    # process player events
    active_scene.process_input(player_events)
    
    # -------------------------------------------------- #
    # update active scene
    # -------------------------------------------------- #
    active_scene.update()
    
    # -------------------------------------------------- #
    # Render
    # -------------------------------------------------- #
    active_scene.render(screen)
    pygame.display.update()
    
    # -------------------------------------------------- #
    # switch to the next scene
    # -------------------------------------------------- # 
    active_scene = active_scene.next
    
#shut'er down
pygame.quit()
sys.exit()
